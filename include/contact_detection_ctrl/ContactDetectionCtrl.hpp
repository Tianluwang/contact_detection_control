/*
 * Copyright (c) 2016, Jan Carius
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Autonomous Systems Lab, ETH Zurich nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Jan Carius BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*!
 * @file     ContactDetectionCtrl.hpp
 * @author   Jan Carius
 * @date     Oct, 2016
 */

#pragma once

// std library
#include <string>
#include <vector>
#include <atomic>
#include <thread>
#include <math.h>

//yaml parsing
#include <yaml-cpp/yaml.h>

// roco
#include <roco_ros/controllers/ControllerRos.hpp>

// eigen
#include <Eigen/Core>
#include <Eigen/SVD>


// huskanypulator model
#include <huskanypulator_roco/RocoState.hpp>
#include <huskanypulator_roco/RocoCommand.hpp>
#include <huskanypulator_msgs/EEstate.h>

// parameter handling
#include <parameter_handler_ros/parameter_handler_ros.hpp>

// utils
#include <robot_utils/controllers/PidEigen.hpp>
#include <robot_utils/timers/ChronoTimer.hpp>

// ros
#include <tf/transform_listener.h>
#include <std_srvs/Empty.h>

#include <contact_detection_ctrl/throttled_ros_publisher.hpp>

namespace contact_detection_ctrl {

class ContactDetectionCtrl: virtual public roco_ros::ControllerRos<huskanypulator_roco::RocoState, huskanypulator_roco::RocoCommand> {
 public:
    using Base = roco_ros::ControllerRos<huskanypulator_roco::RocoState, huskanypulator_roco::RocoCommand>;

 public:
    //! Constructor
    ContactDetectionCtrl();

    //! Destructor
    virtual ~ContactDetectionCtrl();

 protected:
    //! Roco implementation
    virtual bool create(double dt);
    virtual bool initialize(double dt);
    virtual bool advance(double dt);
    virtual bool cleanup();
    virtual bool reset(double dt);
    virtual bool preStop();
    virtual bool stop();

 private:
    /*
     * #####
     * Member variables and constants
     * #####
     */

    ros::NodeHandle nh_;

    // Parameter parsing object
    YAML::Node controlParams_;

    //additional model object used for inverse kinematics
    huskanypulator_model::HuskanypulatorModel invKinModel_;
    std::thread invKinThread_;

    /// Dimensions
    //# State parameters
    //# n_dof = joint space DoF (system DOF): 6+6+2
    //# n_qt = joint space dimension with quaternion representation 7+6+2
    //# n_act = number of actuated joints 0+6+2
    //# m_dof = task space DoF 3+3
    //# m_qt = task space dimension with quaternion representation 3+4
    static const int n_dof_ = huskanypulator_description::internal::numGenCoordWholeBody;
    static const int n_qt_  = huskanypulator_description::internal::numGenCoordWholeBodyQuaternion;
    static const int n_act_ = huskanypulator_description::internal::numGenCoordJoints;
    static const int m_dof_ = 6;
    static const int m_qt_  = 7;

    /// Toggle emergency stop
    std::atomic<bool> runInvKin_;
    std::atomic<bool> invKinConverged_;

    // Subscribers
    std::shared_ptr<tf::TransformListener> tfListener_;

    // Publishers
    std::atomic<bool> publishEEPose_, publishEETargetPose_, publishEEPoseError_,publishDirectCalculation_,publishGenMomentum_,publishFirstOrderResidual_,publishSecondOrderResidual_,publishRealExternalTorque_;
    throttled_publisher::Publisher eePosePublisher_;
    throttled_publisher::Publisher eeTargetPosePublisher_;
    throttled_publisher::Publisher eePoseErrorLinearPublisher_;
    throttled_publisher::Publisher eePoseErrorAngularPublisher_;
    throttled_publisher::Publisher directCalculationPublisher_;
    throttled_publisher::Publisher genMomentumPublisher_;
    throttled_publisher::Publisher firstOrderResidualPublisher_;
    throttled_publisher::Publisher secondOrderResidualPublisher_;
    throttled_publisher::Publisher realExternalTorquePublisher_;
    // Services
    ros::ServiceClient resetDesiredEEState_;

    //Frames
    std::string robotFrameId_; //name of robot base link frame in tf


    /// Control parameters
    double dt_ = 0.0; //[s] current time step (time different to previous control update)
    double dt_prev_ = 0.0; //[s] previous time step
    double tauMax_ = 0.0; //[Nm] maximum torque value
    double maxErrorThreshold_ = 0.0;//[m] if target distance greater than threshold, cap to this distance
    double angleErrorScale_ = 0.0; //[m/rad] weighting of angular error to position error
    double twistTimeoutMax_ = 0.0; //[s] max timeout for new twist command.

    // desired and actual EE state (task space)
    huskanypulator_model::Pose xdes_in_B_; // desired EE pose w.r.t. base frame
    huskanypulator_model::Twist xdes_in_B_dot_; //desired EE twist in base frame
    huskanypulator_model::Pose xee_in_B_; //current EE pose w.r.t. base frame


/////////////////// 2017.04.25 Define system dynamics & arm-joints-only project matrix/////////////

    /// system dynamics
    Eigen::MatrixXd MassInertiaMatrix_;
    Eigen::MatrixXd MassInertiaMatrix_AJO_;
    Eigen::VectorXd NonlinearEffects_;
    Eigen::VectorXd NonlinearEffects_AJO_;


    Eigen::Matrix<double, 2, 2> two_dof_mass;
    Eigen::Matrix<double, 2, 2> two_dof_mass_prev;
    Eigen::Matrix<double, 2, 2> two_dof_cc;
    Eigen::Matrix<double, 2, 1> two_dof_gg;
    Eigen::Matrix<double, 2, 1> two_dof_vel;



    Eigen::Matrix<double, 2, 2> two_dof_jacobian;


    /// Arm-joints-only projection matrix
    Eigen::Matrix<double, 14, 6> AJO_projection_;




    /// Jacobians
    Eigen::MatrixXd J_ee_in_B_, J_ee_in_B_dot_;
    Eigen::MatrixXd J_ee_in_B_AJO_, J_ee_in_B_dot_AJO_; //arm joints only
    Eigen::MatrixXd J_ee_in_B_AJO_inv_;





    // Actuator torques
    Eigen::VectorXd tau_command_;
    double alpha_ = 0.01;

    std::mutex desiredVelPosMutex_;
    Eigen::Matrix<double, 6, 1> desiredJointVel_, desiredJointPos_, desiredJointPosFiltered_, desiredJointVelFiltered_;


//////////////////////////////////2017.4.23//////////////////////////////////////////////////////////////


    /// Contact detection related quantities
    Eigen::Matrix<double, 2, 1> genMomentum_AJO_;
    Eigen::Matrix<double, 2, 1> genMomentum_AJO_prev_;
    Eigen::Matrix<double, 2, 1> external_torque_;
    //////2017.4.27////
    Eigen::Matrix<double, 2, 1> first_order_residual_;
    Eigen::Matrix<double, 2, 1> first_order_residual_prev_;
    Eigen::Matrix<double, 2, 1> intergral_term_;
    Eigen::Matrix<double, 2, 1> intergral_term_prev_;

    Eigen::Matrix<double, 2, 1> second_order_residual_;
    Eigen::Matrix<double, 2, 1> second_order_residual_prev_;
    Eigen::Matrix<double, 2, 1> intergral_term_two_;
    Eigen::Matrix<double, 2, 1> intergral_term_two_prev_;


    Eigen::Matrix<double, 2, 1> tau_readings_;


    Eigen::Matrix<double, 6, 1> external_force;  // the 6 dof force applied to the end-effector

    Eigen::Matrix<double, 6, 1> real_external_torque;

    // the real external torque on the two joints




/////////////////////////////////2017.4.25///////////////////////////////////////

    /// Add the item for previous joint position
    Eigen::Matrix<double, 2, 1> jointPosition_prev_;


    Eigen::Matrix<double, 2, 1> genMomentum_AJO_integral_;
    Eigen::Matrix<double, 2, 1> genMomentum_AJO_dot_plusExtF_prev_;
    Eigen::Matrix<double, 2, 1> residual_, residual_prev_;


    huskanypulator_model::Wrench directWrench_;


    parameter_handler::Parameter<double> residualGain_;
    Eigen::Matrix<double, 2, 1> jointVelocities_AJO_prev_;
    Eigen::Matrix<double, 2, 1> tau_readings_AJO_prev_;
    Eigen::MatrixXd MassInertiaMatrix_AJO_prev_, MassInertiaMatrix_AJO_prevprev_;
    Eigen::VectorXd NonlinearEffects_AJO_prev_;
    double contactWrenchThreshold_ = 0.0;

///////////////////////////////2017.4.29 6 dof system ////////////////////////////

    Eigen::Matrix<double, 6, 6> six_dof_cc;
    Eigen::Matrix<double, 6, 1> six_dof_gg;



///////////////////////////// 2017.5.2 matrix for LQR controller /////////////////////

    Eigen::Matrix<double, 4, 4> lqr_a;
    Eigen::Matrix<double, 4, 2> lqr_b;
    Eigen::Matrix<double, 4, 4> lqr_q;
    Eigen::Matrix<double, 2, 2> lqr_r;
    Eigen::Matrix<double, 2, 2> two_dof_mass_inv;
    Eigen::Matrix<double, 4, 1> state_;
    Eigen::Matrix<double, 2, 1> two_dof_tau_command_;

    Eigen::Matrix<double, 2, 4> K_;

    Eigen::Matrix<double, 4, 4> lqr_p_i_1;
    Eigen::Matrix<double, 4, 4> lqr_p_i;
    Eigen::Matrix<double, 2, 2> container_;

    Eigen::Matrix<double, 2, 2> two_dof_mass_former;

    double joint_2_posi_prev = 0.0;
///////////////////////////////// 2017.5.5 for new C matrix calculated from rbdl model/////////////

    Eigen::VectorXd non_linear_term;
    Eigen::VectorXd non_linear_model;
    Eigen::Matrix<double, 2, 2> two_dof_vel_matrix;

    // Timer
    robot_utils::HighResolutionClockTimer chronoTimer_;

    // Joints to unfreeze
    std::vector<int> unfreeze_joints_;

    /*
     * #####
     * Member functions
     * #####
     */

    /*
     * Creation and initialization of subscribers, publishers, services.
     */
    void initSubscribers();
    void initPublishers();
    void initServices();

    /*
     * parameter parsing
     */
    bool loadParameters(const std::string& filepath);

    /*
     * Called each time step. Top level procedure to calculate and finally send actuator commands.
     */
    bool controlLoop();

    /*
     * Compute forward kinematics.
     * Assigns the end-effector pose (w.r.t. base frame) to the pose argument
     */
    void eeFwdKin(huskanypulator_model::Pose& pose) const;

    /*
     * Calculates difference in pose represented by a position difference and rotation vector
     * Assumes poses to be of the form:
     *   poseA: position = r_IA_in_I, rotation = R_IA
     *   poseB: position = r_IB_in_I, rotation = R_IB
     * The reference frame I may be arbitrary but must be the same for both.
     *
     * Result assigned to vector poseDiff of length 6:
     *  first 3 elements: R_BA as rotation vector represented in I (= Rvec_BA_in_I)
     *  last 3 elements: position error = r_BA_in_I
     *
     * Usage: poseDiff(actual, target, poseDiff) -> action needs to be in direction AGAINST the errors
     */
    void poseDiff(huskanypulator_model::Pose poseA,
                  huskanypulator_model::Pose poseB,
                  Eigen::Matrix<double,6,1>& poseDiff) const;


///////////2017.04.25 Add updateKinDynTerms//////

    void updateKinDynTerms();







    /*
     * Set calculated actuator commands
     */
    bool setActuatorCommands(Eigen::VectorXd& tau);

    /*
     * Apply torque limits (sanity checks, safety)
     */
    bool applyTorqueLimits(Eigen::VectorXd& tau);

    /*
     * Set desired target to internal target variables (xd_ etc).
     */
    bool gotoTarget();

    /*
     * Compute damped pseudoinverse of a matrix
     * adapted from http://wiki.icub.org/codyco/dox/html/namespacecodyco_1_1math.html
     */
    template <typename DerivedA, typename DerivedApinv>
    bool dampedPseudoInverse(const Eigen::MatrixBase<DerivedA>& A,
                             Eigen::MatrixBase<DerivedApinv>& Apinv,
                             const double tolerance,
                             const double maxDampingFactor,
                             const unsigned int computationOptions = Eigen::ComputeThinU|Eigen::ComputeThinV);

    /*
     * convenience method to access the rbdl model
     */
    inline huskanypulator_model::HuskanypulatorModel* getModel() const {return getState().getHuskanypulatorModelPtr();}

    /*
     * Publishes EE Pose/ EE Target Pose
     */
    void publishEEPose();
    void publishEETargetPose();
    void publishEEPoseError(const Eigen::Matrix<double,6,1>& poseError);

    /*
     * Helper functions, not used at the moment but we will keep them here for now.
     */

    /*
     * Calculates a rotation matrix R_AC such that the x-axis of the C frame
     * coincides with the vector vec, which is assumed to be represented in the A frame.
     * The orientation of the other two axes of A in the plane orthogonal to vec are arbitrary.
     */

    ///////////////////////////////////// 2017.4.23 //////////////////////////////////////////////////
    void publishDirectCalculation();
    void publishGenMomentum();
    //////////////////////////////////// 2017.4.27 //////////////////////////////////////////////////
    void publishFirstOrderResidual();
    void publishSecondOrderResidual();

    ///////////////////////////////////  2017.4.28 //////////////////////////////////////////////////

    void publishRealExternalTorque();


    void getRotmatFromVector(const Eigen::Vector3d& vec, Eigen::Matrix3d& rotMat) const;

    /*
     * Online inverse kinematics function (Siciliano 3.7, p. 132)
     * Should be ran in a separate thread, does not return unless requested.
     */
    void invKin();
};

} /* namespace contact_detection_ctrl */
