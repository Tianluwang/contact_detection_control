
#pragma once

#include <assert.h>
#include <ros/publisher.h>
#include <ros/time.h>

namespace throttled_publisher {

class Publisher : public ros::Publisher {

public:
  typedef ros::Publisher Base;

  Publisher() : Base(),
                lastPubTime_(0.0),
                period_(0.1) {}

  Publisher(Base rhs, double rate) : Base(rhs),
                                     lastPubTime_(0.0) {
    assert(rate>0.0);
    period_ = ros::Duration(1.0 / rate);
  }

  template <typename M>
  void publish(const boost::shared_ptr<M>& message) {
    const auto now = ros::Time::now();
    if(timeCondition(now)){
      Base::publish(message);
      lastPubTime_ = now;
    }
  }

  template <typename M>
  void publish(const M& message) {
    const auto now = ros::Time::now();
    if(timeCondition(now)){
      Base::publish(message);
      lastPubTime_ = now;
    }
  }

private:
  inline bool timeCondition(const ros::Time& time) const {
    return ((time-lastPubTime_) > period_);
  }

private:
  ros::Duration period_;
  ros::Time lastPubTime_;
};

} // namespace throttled_publisher
