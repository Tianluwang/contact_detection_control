/*
 * Copyright (c) 2016, Jan Carius
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Autonomous Systems Lab, ETH Zurich nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Jan Carius BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*!
 * @file       ContactDetectionCtrl.cpp
 * @author     Jan Carius, Martin Wermelinger, Tianlu Wang
 * @date       Apr, 2017
 */
#include <math.h>
// arm controller
#include <contact_detection_ctrl/ContactDetectionCtrl.hpp>

#include <message_logger/message_logger.hpp>

// rocoma_plugin
#include <rocoma_plugin/rocoma_plugin.hpp>

// messages
#include <geometry_msgs/WrenchStamped.h>
#include <kindr_msgs/VectorAtPosition.h>
#include <std_msgs/Float32.h>
// export controller plugin
ROCOMA_EXPORT_CONTROLLER_ROS(ContactDetectionCtrlPlugin,
                             huskanypulator_roco::RocoState,
                             huskanypulator_roco::RocoCommand,
                             contact_detection_ctrl::ContactDetectionCtrl)

namespace contact_detection_ctrl {

ContactDetectionCtrl::ContactDetectionCtrl()
    : Base(),
      runInvKin_(true),
      invKinConverged_(false),
      publishEEPose_(false),
      publishEETargetPose_(false),
      publishEEPoseError_(false),
      publishDirectCalculation_(true),
	  publishGenMomentum_(true),
	  publishFirstOrderResidual_(true),
	  publishSecondOrderResidual_(true),
	  publishRealExternalTorque_(true)
{
  nh_ = getNodeHandle();
  this->name_ = "ContactDetectionCtrl";
}

ContactDetectionCtrl::~ContactDetectionCtrl() {
}

bool ContactDetectionCtrl::create(double dt) {
  dt_ = dt;

  std::string huskanypulator2w_urdf;
  getNodeHandle().getParam("huskanypulator_description_2w", huskanypulator2w_urdf);
  invKinModel_.initModelFromUrdfString(huskanypulator2w_urdf);

  MELO_INFO_STREAM("Controller " << this->getName() << " is created!");
  return true;
}

bool ContactDetectionCtrl::initialize(double dt) {

  // load parameters
  std::string parameter_path = this->getParameterPath();
  std::string file_path = parameter_path + "config/controlParams.yaml";
  MELO_INFO_STREAM("ContactDetectionCtrl filepath: " << file_path);
  if(!loadParameters(file_path)){
    MELO_ERROR("[ContactDetectionCtrl] Could not read all parameters. Initialization aborted.");
    return false;
  }
/////////////////////2017.04.25 assign initial values to member variables//////


  //assign initial values to member variables
  J_ee_in_B_ = Eigen::MatrixXd::Zero(6,n_dof_);
  J_ee_in_B_dot_ = Eigen::MatrixXd::Zero(6,n_dof_);
  J_ee_in_B_AJO_ = Eigen::MatrixXd::Zero(6,6);
  J_ee_in_B_AJO_inv_ = Eigen::MatrixXd::Zero(6,6);
  MassInertiaMatrix_ = Eigen::MatrixXd::Zero(n_dof_,n_dof_);
  MassInertiaMatrix_AJO_ = Eigen::MatrixXd::Zero(6,6);
  MassInertiaMatrix_AJO_prev_ = MassInertiaMatrix_AJO_prevprev_ = Eigen::MatrixXd::Zero(6,6);
  ///////////////////2017.5.7 Nonlinear term must be initialized ///////
  NonlinearEffects_ = Eigen::VectorXd::Zero(n_dof_);
  NonlinearEffects_AJO_ = Eigen::VectorXd::Zero(6);

  two_dof_mass.setZero();
  two_dof_mass_prev.setZero();

  two_dof_cc.setZero();
  two_dof_gg.setZero();

  two_dof_vel.setZero();

  tau_readings_.setZero();


  two_dof_jacobian.setZero();

  AJO_projection_.setZero();
  AJO_projection_.block<6,6>(6,0).setIdentity();
  genMomentum_AJO_.setZero();
  genMomentum_AJO_integral_.setZero();
  genMomentum_AJO_dot_plusExtF_prev_.setZero();
  residual_.setZero();
  residual_prev_.setZero();
  jointVelocities_AJO_prev_.setZero();
  tau_readings_AJO_prev_.setZero();
  // 2017.04.25 initial position for the joint
  jointPosition_prev_[1] = 0.0;
  jointPosition_prev_[2] = 1.57;
  // 2017.4.25 set the genMomentum_prev zero, set the initial external toque zero, joint torque zero
  genMomentum_AJO_prev_.setZero();
  external_torque_.setZero();
//  tau_readings_AJO_.setZero();
  //2017.4.26 set the initial value for first order residual, which is 0
  first_order_residual_.setZero();
  first_order_residual_prev_.setZero();

  intergral_term_.setZero();
  intergral_term_prev_.setZero();

  //2017.4.27 set the initial value for second order residual, which is 0
  second_order_residual_.setZero();
  second_order_residual_prev_.setZero();

  intergral_term_two_.setZero();
  intergral_term_two_prev_.setZero();

  real_external_torque.setZero();

  external_force(0,0) = 0;
  external_force(1,0) = 0;
  external_force(2,0) = 0;
  external_force(3,0) = 0;
  external_force(4,0) = 0;
  external_force(5,0) = 5;

/////////////////////// 2017.4.29 6 dof system ////
//  c_i_j_k_.setZero();



  lqr_a.setZero();
  lqr_b.setZero();
  lqr_q.setZero();
  lqr_r.setZero();
  two_dof_mass_inv.setZero();


  K_.setZero();
  state_.setZero();
  two_dof_tau_command_.setZero();

  lqr_p_i_1.setZero();

  lqr_p_i_1(0,0) = 1000;
  lqr_p_i_1(1,1) = 500;
  lqr_p_i_1(2,2) =  300;
  lqr_p_i_1(3,3) =  10;

  lqr_p_i.setZero();
  container_.setZero();
  two_dof_mass_former.setZero();


  non_linear_term.setZero();
  non_linear_model.setZero();

  two_dof_vel_matrix.setZero();



  initServices();
  initSubscribers();
  initPublishers();


  if(!resetDesiredEEState_.waitForExistence(ros::Duration(1.0))){
    return false;
  }

  std_srvs::Empty srv;
  resetDesiredEEState_.call(srv);
  ros::Duration(1.0).sleep();

  eeFwdKin(xee_in_B_);
  xdes_in_B_ = xee_in_B_;
  xdes_in_B_dot_.setZero();
  dt_ = dt;
  dt_prev_ = dt_;
  tau_command_.resize(6);
  tau_command_.setZero();

  invKinConverged_ = false;
  runInvKin_ = true;
  invKinThread_ = std::thread{&ContactDetectionCtrl::invKin, this};

  ros::Time tick = ros::Time::now();
  do {
    if (ros::Time::now() - tick > ros::Duration(4.0)) {
      MELO_ERROR(
          "[ContactDetectionCtrl] Controller could not be initialized. InvKin not converged.");
      return false;
    }
    ros::Duration(0.2).sleep();
  } while(!invKinConverged_);

  MELO_INFO_STREAM("Controller " << this->getName() << " is initialized!");
  return true;
}

bool ContactDetectionCtrl::advance(double dt) {
  dt_prev_ = dt_;
  dt_ = dt;

  if (!invKinConverged_) {
    MELO_WARN("[ContactDetectionCtrl::advance] Inverse kinematics not converged.");
    return false;
  }

  try{
    return controlLoop();
  }catch(const std::exception& e){
    MELO_ERROR_STREAM("[ContactDetectionCtrl::advance] caught exception in controlLoop:\n"
        << e.what());
    return false;
  }catch(...){
    MELO_ERROR_STREAM("[ContactDetectionCtrl::advance] caught generic exeption in controlLoop");
    return false;
  }
}

bool ContactDetectionCtrl::cleanup() {
  return true;
}

bool ContactDetectionCtrl::reset(double dt) {
  return initialize(dt);
}

bool ContactDetectionCtrl::preStop() {

  //deregister services
  resetDesiredEEState_.shutdown();

  //stop publishers
  publishEEPose_ = false;
  publishEETargetPose_ = false;
  publishEEPoseError_ = false;
  publishDirectCalculation_ = false;
  publishGenMomentum_ = false;
  publishFirstOrderResidual_ = false;
  publishSecondOrderResidual_ = false;
  publishRealExternalTorque_ = false;
  return true;
}

bool ContactDetectionCtrl::stop() {

  eePosePublisher_.shutdown();
  eeTargetPosePublisher_.shutdown();
  eePoseErrorLinearPublisher_.shutdown();
  eePoseErrorAngularPublisher_.shutdown();
  directCalculationPublisher_.shutdown();
  genMomentumPublisher_.shutdown();
  firstOrderResidualPublisher_.shutdown();
  secondOrderResidualPublisher_.shutdown();
  realExternalTorquePublisher_.shutdown();

  runInvKin_ = false;
  invKinThread_.join();
  invKinConverged_ = false;
  return true;
}

void ContactDetectionCtrl::initSubscribers() {
  tfListener_.reset(new tf::TransformListener);
}

void ContactDetectionCtrl::initPublishers() {
  if(publishEEPose_){
    eePosePublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<geometry_msgs::PoseStamped>("eePose", 10),
                                                      5.0);
  }
  if(publishEETargetPose_){
    eeTargetPosePublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<geometry_msgs::PoseStamped>("eeTargetPose", 10),
                                                            5.0);
  }
  if(publishEEPoseError_){
    eePoseErrorLinearPublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<kindr_msgs::VectorAtPosition>("eePoseErrorLinear", 10),
                                                                 5.0);
    eePoseErrorAngularPublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<kindr_msgs::VectorAtPosition>("eePoseErrorAngular", 10),
                                                                  5.0);
  }
  if(publishDirectCalculation_){
    directCalculationPublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<geometry_msgs::Vector3Stamped>("directCalculation", 10),
                                                              5.0);
  }
  if(publishGenMomentum_){
	genMomentumPublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<geometry_msgs::Vector3Stamped>("genMomentum", 10),
                                                              5.0);
  }

  if(publishFirstOrderResidual_){
	firstOrderResidualPublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<geometry_msgs::Vector3Stamped>("firstOrderResidual", 10),
                                                              5.0);
  }

  if(publishSecondOrderResidual_){
	secondOrderResidualPublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<geometry_msgs::Vector3Stamped>("secondOrderResidual", 10),
                                                              5.0);
  }
  if(publishRealExternalTorque_){
	realExternalTorquePublisher_ = throttled_publisher::Publisher(getNodeHandle().advertise<geometry_msgs::Vector3Stamped>("realExternalTorque", 10),
                                                              5.0);
  }

}

void ContactDetectionCtrl::initServices() {
  resetDesiredEEState_ = getNodeHandle().serviceClient<std_srvs::Empty>("resetDesiredEEState");
}

bool ContactDetectionCtrl::loadParameters(const std::string& filepath) {

  //load the configuration file
  controlParams_ =  YAML::LoadFile(filepath);
  if (controlParams_.IsNull()){
    MELO_WARN("[TrajectoryController::loadParameters]: Could not load parameter file %s!",
                  filepath.c_str());
    return false;
  }

  //configure the parameter handler
  parameter_handler_ros::setParameterHandlerRos(&nh_);

  publishEEPose_         = controlParams_["publishers"]["publishEEPose"].as<bool>();
  publishEETargetPose_   = controlParams_["publishers"]["publishEETargetPose"].as<bool>();
  publishEEPoseError_    = controlParams_["publishers"]["publishEEPoseError"].as<bool>();
  publishDirectCalculation_    = controlParams_["publishers"]["publishDirectCalculation"].as<bool>();
  publishGenMomentum_    = controlParams_["publishers"]["publishGenMomentum"].as<bool>();
  publishFirstOrderResidual_    = controlParams_["publishers"]["publishFirstOrderResidual"].as<bool>();
  publishSecondOrderResidual_    = controlParams_["publishers"]["publishSecondOrderResidual"].as<bool>();
  publishRealExternalTorque_    = controlParams_["publishers"]["publishRealExternalTorque"].as<bool>();

  robotFrameId_ = controlParams_["robotFrameId"].as<std::string>();
  tauMax_ = controlParams_["tauMax"].as<double>();
  twistTimeoutMax_ = controlParams_["twistTimeoutMax"].as<double>();

  maxErrorThreshold_ = controlParams_["targetTracking"]["threshold"].as<double>();
  angleErrorScale_ = controlParams_["targetTracking"]["angleErrorScale"].as<double>();

  unfreeze_joints_ = controlParams_["unfreezeJoints"].as<std::vector<int>>();

  return true;
}

void ContactDetectionCtrl::eeFwdKin(huskanypulator_model::Pose& pose) const {
  huskanypulator_model::Position r_base_ee_in_base(
      getModel()->getPositionBodyToBody(huskanypulator_model::BodyEnum::BASE,
                                        huskanypulator_model::BodyEnum::TOOL,
                                        huskanypulator_model::CoordinateFrame::BASE));

  huskanypulator_model::RotationMatrix R_Base_World(
      getModel()->getOrientationWorldToBody(huskanypulator_model::BodyEnum::BASE));
  huskanypulator_model::RotationMatrix R_EE_World(
      getModel()->getOrientationWorldToBody(huskanypulator_model::BodyEnum::TOOL));

  pose.getPosition() = r_base_ee_in_base;
  pose.getRotation() = huskanypulator_model::RotationQuaternion(R_Base_World*(R_EE_World.transposed()));
}

void ContactDetectionCtrl::poseDiff(huskanypulator_model::Pose poseA,
                                              huskanypulator_model::Pose poseB,
                                              Eigen::Matrix<double,6,1>& poseDiff) const {

  poseDiff.tail<3>() = (poseA.getPosition() - poseB.getPosition()).toImplementation();

  poseB.getRotation().fix();
  poseA.getRotation().fix();
  huskanypulator_model::RotationVector rotationDiff(poseB.getRotation().inverted() * poseA.getRotation());
  if (std::abs(std::abs(poseB.getRotation().getDisparityAngle(poseA.getRotation())) - rotationDiff.toImplementation().norm()) > 0.01) {
    poseA.getRotation().w() *= -1.0;
    poseA.getRotation().x() *= -1.0;
    poseA.getRotation().y() *= -1.0;
    poseA.getRotation().z() *= -1.0;
    poseA.getRotation().fix();

    rotationDiff = poseB.getRotation().inverted() * poseA.getRotation();
  }
  poseDiff.head<3>() = poseB.getRotation().rotate(rotationDiff.vector());
}

template <typename DerivedA, typename DerivedApinv>
bool ContactDetectionCtrl::dampedPseudoInverse(const Eigen::MatrixBase<DerivedA>& A,
                                                         Eigen::MatrixBase<DerivedApinv>& Apinv,
                                                         const double tolerance,
                                                         const double maxDampingFactor,
                                                         const unsigned int computationOptions) {

  if (computationOptions == 0) return -1;

  Eigen::JacobiSVD<Eigen::MatrixXd::PlainObject> Asvd;
  Asvd.compute(A, computationOptions);

  //singular values are ordered from largest to smallest (=> zeros at the end)
  Eigen::JacobiSVD<Eigen::MatrixXd::PlainObject>::SingularValuesType singularValues = Asvd.singularValues();
  double dampingFactor = 0.0;
  if(Asvd.nonzeroSingularValues() == 0){
    MELO_WARN("[ContactDetectionCtrl::dampedPseudoInverse] All singular values are zero");
  }else{
    const double minSingularValue = singularValues(Asvd.nonzeroSingularValues()-1);
    if (minSingularValue < tolerance) {
      dampingFactor = (1.0 - (minSingularValue/tolerance)*(minSingularValue/tolerance) * maxDampingFactor);
    }
  }

  Eigen::JacobiSVD<Eigen::MatrixXd::PlainObject>::SingularValuesType singularValuesInverted = singularValues;
  //looping only through nonzero singular values
  for (int idx = 0; idx < Asvd.nonzeroSingularValues(); idx++) {
    singularValuesInverted(idx) = singularValues(idx) / ((singularValues(idx) * singularValues(idx)) + (dampingFactor * dampingFactor));
  }
  Apinv = Asvd.matrixV() * singularValuesInverted.asDiagonal() * Asvd.matrixU().transpose();
  return true;
}

////////////////////////////////2017.04.25 Adding the updateKinDynTerms ///////
void ContactDetectionCtrl::updateKinDynTerms(){
  //TODO: acquire mutex

  /*
   * forward kinematics
   */
  eeFwdKin(xee_in_B_);

  /*
   * get Jacobians and respective inverses.
   * Note: J_ee_in_B_ is temporarily in world frame and is rotated afterwards.
   */
  chronoTimer_.pinTime("updateKinDyn/JacSpatial");
  J_ee_in_B_.setZero();
  getModel()->getJacobianSpatialWorldToBody(
      J_ee_in_B_,
      huskanypulator_model::BranchEnum::ARM,
      huskanypulator_model::BodyNodeEnum::TOOL,
      huskanypulator_model::CoordinateFrame::WORLD);
  chronoTimer_.splitTime("updateKinDyn/JacSpatial");

  chronoTimer_.pinTime("updateKinDyn/JacSpatialTimeDerivative");
  J_ee_in_B_dot_.setZero();
  getModel()->getJacobianSpatialTimeDerivativeWorldToBody(
        J_ee_in_B_dot_,
        J_ee_in_B_,
        huskanypulator_model::BranchEnum::ARM,
        huskanypulator_model::BodyNodeEnum::TOOL,
        huskanypulator_model::CoordinateFrame::BASE);
  chronoTimer_.splitTime("updateKinDyn/JacSpatialTimeDerivative");

  Eigen::Matrix3d rotationWorldToBase = getModel()->getOrientationWorldToBody(
      huskanypulator_model::BodyEnum::BASE);
  J_ee_in_B_.topRows(3) = rotationWorldToBase * J_ee_in_B_.topRows(3);
  J_ee_in_B_.bottomRows(3) = rotationWorldToBase * J_ee_in_B_.bottomRows(3);


  J_ee_in_B_AJO_ = J_ee_in_B_ * AJO_projection_;
  J_ee_in_B_dot_AJO_ = J_ee_in_B_dot_ * AJO_projection_;
//  chronoTimer_.pinTime("updateKinDyn/Pseudoinverse");
//  dampedPseudoInverse(J_ee_in_B_AJO_, J_ee_in_B_AJO_inv_, pseudoInvTol_, pseudoInvDamping_);
//  chronoTimer_.splitTime("updateKinDyn/Pseudoinverse");

  /*
   * get EoM terms
   */
  chronoTimer_.pinTime("updateKinDyn/EoMTerms");
  MassInertiaMatrix_.setZero();
  getModel()->getMassInertiaMatrix(MassInertiaMatrix_);
  MassInertiaMatrix_AJO_ = AJO_projection_.transpose() * MassInertiaMatrix_ * AJO_projection_;

  NonlinearEffects_.setZero();
  getModel()->getNonlinearEffects(NonlinearEffects_);
  NonlinearEffects_AJO_ = AJO_projection_.transpose() * NonlinearEffects_;
  chronoTimer_.splitTime("updateKinDyn/EoMTerms");



}






bool ContactDetectionCtrl::applyTorqueLimits(Eigen::VectorXd& tau){
  if(tau.hasNaN()){
    MELO_WARN("[ContactDetectionCtrl::applyTorqueLimits] tau has NaN values. Emergency stop.");
    return false;
  }

  // Monitor maximum value of tau_command_, cap at tauMax_
  for(int i=0; i < tau.size(); i++){
    if (tau(i) > tauMax_){
      MELO_WARN_STREAM("[HuskanypulatorController::applyTorqueLimits] too high positive torque on joint " << i
          << ": " << tau(i));
      tau(i) = tauMax_;
    }else if (tau(i) < -tauMax_){
      MELO_WARN_STREAM("[HuskanypulatorController::applyTorqueLimits] too high negative torque on joint " << i
          << ": " << tau(i));
      tau(i) = -tauMax_;
    }
  }
  return true;
}

bool ContactDetectionCtrl::setActuatorCommands(Eigen::VectorXd& tau) {
  //Some sanity checks first
  if(tau.size() != 6 or !applyTorqueLimits(tau)){
    MELO_WARN("[ContactDetectionCtrl::setActuatorCommands] Commands not valid.");
    return false;
  }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//  std::lock_guard<std::mutex> lock(desiredVelPosMutex_);
//  desiredJointPosFiltered_[1] = desiredJointPosFiltered_[1] + alpha_ * (desiredJointPos_[1] - desiredJointPosFiltered_[1]);
//  desiredJointPosFiltered_[2] = desiredJointPosFiltered_[2] + alpha_ * (desiredJointPos_[2] - desiredJointPosFiltered_[2]);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  for (auto& command : getCommand().getActuatorCommands()) {
    unsigned int jointUInt = getModel()->getJointUIntFromJointString(
        command.getName());
    if (jointUInt < 6u) {
      command.setMode(command.Mode::MODE_FREEZE);
      if (unfreeze_joints_.size() == 0u) continue;
      for (std::size_t i = 0u; i < unfreeze_joints_.size(); ++i) {
        unsigned int unfreezeJointIndex = unfreeze_joints_[i];
        if (unfreezeJointIndex == jointUInt) {
          command.setMode(command.Mode::MODE_JOINT_TORQUE);
          command.setJointTorque(tau(jointUInt));
//          command.setJointPosition(desiredJointPos_(jointUInt));
//          command.setJointVelocity(desiredJointVelFiltered_(jointUInt));
          continue;
        }
      }
    } else {  //wheel joints
      command.setMode(command.Mode::MODE_FREEZE);
    }
  }

  return true;
}

bool ContactDetectionCtrl::controlLoop() {
  try{
    if(!gotoTarget()){
      MELO_ERROR("[ContactDetectionCtrl::controlLoop] gotoTarget failed.");
      return false;
    }
  }catch(const std::exception& e){
    MELO_ERROR_STREAM("[ContactDetectionCtrl::controlLoop] caught exception in gotoTarget:\n"
      << e.what());
    return false;
  }

  huskanypulator_model::JointPositions jointPos = getModel()->getState().getJointPositions();
  huskanypulator_model::JointVelocities jointVel = getModel()->getState().getJointVelocities();


  {
/////////////////////////////////////////// The design of the PD position controller ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	std::lock_guard<std::mutex> lock(desiredVelPosMutex_);
//	ROS_INFO_STREAM("Desired joint pos: " << desiredJointPos_[1]);
//	ROS_INFO_STREAM("Joint pos_1: " << jointPos.vector()(1));
//	ROS_INFO_STREAM("Joint pos_2: " << jointPos.vector()(2));




	//////////////////////////////////// 2 dof PD controller ////////////////////////////////////////////////////////////////////////

    tau_command_[1] = 20 *(0.0 - jointPos.vector()(1)) - 10 * jointVel.vector()(1) + getModel()->getGravityTerms().segment<6>(6)(1);
    tau_command_[2] = 20 *(1.57 - jointPos.vector()(2)) - 10 * jointVel.vector()(2) + getModel()->getGravityTerms().segment<6>(6)(2);

    //////////////////////////////////// 6 dof PD controller ///////////////////////////////////////////////////////////////////////

//  tau_command_[0] = 20 *(0.0 - jointPos.vector()(0)) - 10 * jointVel.vector()(0) + getModel()->getGravityTerms().segment<6>(6)(0);
//  tau_command_[1] = 20 *(0.0 - jointPos.vector()(1)) - 10 * jointVel.vector()(1) + getModel()->getGravityTerms().segment<6>(6)(1);
//  tau_command_[2] = 20 *(1.57 -jointPos.vector()(2)) - 10 * jointVel.vector()(2) + getModel()->getGravityTerms().segment<6>(6)(2);
//  tau_command_[3] = 0.2*(0.0 - jointPos.vector()(3)) - 0.2 * jointVel.vector()(3) + getModel()->getGravityTerms().segment<6>(6)(3);
//  tau_command_[4] = 0.2*(0.0 - jointPos.vector()(4)) - 0.2 * jointVel.vector()(4) + getModel()->getGravityTerms().segment<6>(6)(4);
//  tau_command_[5] = 0.2*(0.0 - jointPos.vector()(5)) - 0.2 * jointVel.vector()(5) + getModel()->getGravityTerms().segment<6>(6)(5);





    //////////////////////////////////// 2 dof LQR controller ////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////// matrix A and B change with dynamics /////////////////////////////////////////////////

//	//////////////////// dynamic elements M, C, g ////
//
//	getModel()->getMassInertiaMatrix(MassInertiaMatrix_);
//	MassInertiaMatrix_AJO_ = AJO_projection_.transpose() * MassInertiaMatrix_ * AJO_projection_;
//
//    two_dof_mass(0,0) = MassInertiaMatrix_AJO_(1,1);
//    two_dof_mass(0,1) = MassInertiaMatrix_AJO_(1,2);
//    two_dof_mass(1,0) = MassInertiaMatrix_AJO_(2,1);
//    two_dof_mass(1,1) = MassInertiaMatrix_AJO_(2,2);
//
//    two_dof_mass_prev(0,0) = MassInertiaMatrix_AJO_prev_(1,1);
//    two_dof_mass_prev(0,1) = MassInertiaMatrix_AJO_prev_(1,2);
//    two_dof_mass_prev(1,0) = MassInertiaMatrix_AJO_prev_(2,1);
//    two_dof_mass_prev(1,1) = MassInertiaMatrix_AJO_prev_(2,2);
//
//    if(jointPos.vector()(2) - jointPosition_prev_[2] < 0.001){
//  	  two_dof_cc(0,0) = 0;
//  	  two_dof_cc(0,1) = 0;
//  	  two_dof_cc(1,0) = 0;
//  	  two_dof_cc(1,1) = 0;
//    }
//    else{
//  	  two_dof_cc(0,0) = 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2)) * jointVel.vector()(2);
//  	  two_dof_cc(1,0) = (( two_dof_mass_prev(1,0) - two_dof_mass(1,0) )/(jointPosition_prev_[1] - jointPos.vector()(1)) - 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2))) * jointVel.vector()(1);
//  	  two_dof_cc(0,1) = 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2)) * jointVel.vector()(1) +  (( two_dof_mass_prev(0,1) - two_dof_mass(0,1) )/(jointPosition_prev_[2] - jointPos.vector()(2)) - 0.5 * ( two_dof_mass_prev(1,1) - two_dof_mass(1,1) )/( jointPosition_prev_[1] - jointPos.vector()(1))) * jointVel.vector()(2);
//  	  two_dof_cc(1,1) = 0;
//    }
//
//    two_dof_gg(0,0)=getModel()->getGravityTerms().segment<6>(6)(1);
//    two_dof_gg(1,0)=getModel()->getGravityTerms().segment<6>(6)(2);
//
//
//    ///////////////////// elements for the lqr controller ////
//    ///// A matrix /////
//
//    two_dof_mass_inv = two_dof_mass.inverse();
//    lqr_a(0,0) =   1;
//    lqr_a(1,1) =   1;
//    lqr_a(0,2) =   0.005;
//    lqr_a(1,3) =   0.005;
//    lqr_a(2,2) = - 0.005 * (two_dof_mass_inv(0,0) * two_dof_cc(0,0) + two_dof_mass_inv(0,1) * two_dof_cc(1,0)) + 1;
//    lqr_a(2,3) = - 0.005 * (two_dof_mass_inv(0,0) * two_dof_cc(0,1) + two_dof_mass_inv(0,1) * two_dof_cc(1,1));
//    lqr_a(3,2) = - 0.005 * (two_dof_mass_inv(1,0) * two_dof_cc(0,0) + two_dof_mass_inv(1,1) * two_dof_cc(1,0));
//    lqr_a(3,3) = - 0.005 * (two_dof_mass_inv(1,0) * two_dof_cc(0,1) + two_dof_mass_inv(1,1) * two_dof_cc(1,1)) + 1;
//
//    //// B matrix //////
//    lqr_b(2,0) =   0.005 * two_dof_mass_inv(0,0);
//    lqr_b(2,1) =   0.005 * two_dof_mass_inv(0,1);
//    lqr_b(3,0) =   0.005 * two_dof_mass_inv(1,0);
//    lqr_b(3,1) =   0.005 * two_dof_mass_inv(1,1);
//
//    //// Q matrix /////
//    lqr_q(0,0) = 1000;
//    lqr_q(1,1) = 300;
//    lqr_q(2,2) =  1000;
//    lqr_q(3,3) =  10;
//
//    //// R matrix /////
//    lqr_r(0,0) =  1;
//    lqr_r(1,1) =  1;
//
//
//    //// Calculate K matrix and update the P matrix /////
//    container_ = lqr_b.transpose() * lqr_p_i_1 * lqr_b + lqr_r;
//    K_ = - container_.inverse() * lqr_b.transpose() * lqr_p_i_1 * lqr_a;
//
//    lqr_p_i = lqr_a.transpose() * lqr_p_i_1 * lqr_a + lqr_q - lqr_a.transpose() * lqr_p_i_1 * lqr_b * container_.inverse() * lqr_b.transpose() * lqr_p_i_1 * lqr_a;
//
//    //// control output tau ////
//    state_(0,0) = jointPos.vector()(1) - 0.0;
//    state_(0,1) = jointPos.vector()(2) - 1.57;
//    state_(0,2) = jointVel.vector()(1);
//    state_(0,3) = jointVel.vector()(2);
//
//    two_dof_tau_command_ = K_ * state_ + two_dof_gg;
//
//    tau_command_[1] = two_dof_tau_command_(0,0);
//    tau_command_[2] = two_dof_tau_command_(1,0);
//
//    ROS_INFO_STREAM("torque command_1: " << two_dof_mass(0,0));
//
//
//    /// items for the next loop ///
//
//    MassInertiaMatrix_AJO_prev_ = MassInertiaMatrix_AJO_;
//    jointPosition_prev_(0,0) = jointPos.vector()(1);
//    jointPosition_prev_(1,0) = jointPos.vector()(2);
//    lqr_p_i_1 = lqr_p_i;


//	//////////////////////////////////////////// matrix at nominal system, B changes with dynamics  /////////////////////////////////////////////////
//  //////////////////// dynamic elements M, C, g ////
//
//  getModel()->getMassInertiaMatrix(MassInertiaMatrix_);
//  MassInertiaMatrix_AJO_ = AJO_projection_.transpose() * MassInertiaMatrix_ * AJO_projection_;
//
//  two_dof_mass(0,0) = MassInertiaMatrix_AJO_(1,1);
//  two_dof_mass(0,1) = MassInertiaMatrix_AJO_(1,2);
//  two_dof_mass(1,0) = MassInertiaMatrix_AJO_(2,1);
//  two_dof_mass(1,1) = MassInertiaMatrix_AJO_(2,2);
//
//
//  two_dof_gg(0,0)=getModel()->getGravityTerms().segment<6>(6)(1);
//  two_dof_gg(1,0)=getModel()->getGravityTerms().segment<6>(6)(2);
//
//
//	    ///////////////////// elements for the lqr controller ////
//	    ///// A matrix /////
//
//  two_dof_mass_inv = two_dof_mass.inverse();
//  lqr_a(0,0) =   1;
//  lqr_a(1,1) =   1;
//  lqr_a(0,2) =   0.005;
//  lqr_a(1,3) =   0.005;
//  lqr_a(2,2) =   1;
//  lqr_a(3,3) =   1;
//
//	    //// B matrix //////
//  lqr_b(2,0) =   0.005 * two_dof_mass_inv(0,0);
//  lqr_b(2,1) =   0.005 * two_dof_mass_inv(0,1);
//  lqr_b(3,0) =   0.005 * two_dof_mass_inv(1,0);
//  lqr_b(3,1) =   0.005 * two_dof_mass_inv(1,1);
//
//	    //// Q matrix /////
//  lqr_q(0,0) = 1000;
//  lqr_q(1,1) =  500;
//  lqr_q(2,2) =  300;
//  lqr_q(3,3) =  10;
//
//	    //// R matrix /////
//  lqr_r(0,0) =  500;
//  lqr_r(1,1) =  500;
//
//      //// Calculate K matrix and update the P matrix /////
//  container_ = lqr_b.transpose() * lqr_p_i_1 * lqr_b + lqr_r;
//  K_ = - container_.inverse() * lqr_b.transpose() * lqr_p_i_1 * lqr_a;
//
//  lqr_p_i = lqr_a.transpose() * lqr_p_i_1 * lqr_a + lqr_q - lqr_a.transpose() * lqr_p_i_1 * lqr_b * container_.inverse() * lqr_b.transpose() * lqr_p_i_1 * lqr_a;
//
//      //// control output tau ////
//  state_(0,0) = jointPos.vector()(1) - 0.0;
//  state_(0,1) = jointPos.vector()(2) - 1.57;
//  state_(0,2) = jointVel.vector()(1);
//  state_(0,3) = jointVel.vector()(2);
//
//  two_dof_tau_command_ = K_ * state_ + two_dof_gg;
//
//  tau_command_[1] = two_dof_tau_command_(0,0);
//  tau_command_[2] = two_dof_tau_command_(1,0);
//
//  ROS_INFO_STREAM("torque command_1: " << two_dof_mass(0,0));
//
//
//      /// items for the next loop ///
//
//  lqr_p_i_1 = lqr_p_i;
//

////////////////////////////////////////////// Real Nominal system dynamics ////////////////////////////////////////////
//	  //////////////////// dynamic elements M, C, g ////
//
//
//  if(two_dof_mass_former(0,0)==0){
//
//    getModel()->getMassInertiaMatrix(MassInertiaMatrix_);
//	MassInertiaMatrix_AJO_ = AJO_projection_.transpose() * MassInertiaMatrix_ * AJO_projection_;
//
//	two_dof_mass(0,0) = MassInertiaMatrix_AJO_(1,1);
//    two_dof_mass(0,1) = MassInertiaMatrix_AJO_(1,2);
//    two_dof_mass(1,0) = MassInertiaMatrix_AJO_(2,1);
//    two_dof_mass(1,1) = MassInertiaMatrix_AJO_(2,2);
//
//  }
//  else{
//
//	  two_dof_mass = two_dof_mass_former;
//  }
//
//  two_dof_gg(0,0)=getModel()->getGravityTerms().segment<6>(6)(1);
//  two_dof_gg(1,0)=getModel()->getGravityTerms().segment<6>(6)(2);
//
//
//		    ///////////////////// elements for the lqr controller ////
//		    ///// A matrix /////
//
//  two_dof_mass_inv = two_dof_mass.inverse();
//  lqr_a(0,0) =   1;
//  lqr_a(1,1) =   1;
//  lqr_a(0,2) =   0.005;
//  lqr_a(1,3) =   0.005;
//  lqr_a(2,2) =   1;
//  lqr_a(3,3) =   1;
//
//		    //// B matrix //////
//  lqr_b(2,0) =   0.005 * two_dof_mass_inv(0,0);
//  lqr_b(2,1) =   0.005 * two_dof_mass_inv(0,1);
//  lqr_b(3,0) =   0.005 * two_dof_mass_inv(1,0);
//  lqr_b(3,1) =   0.005 * two_dof_mass_inv(1,1);
//
//		    //// Q matrix /////
//  lqr_q(0,0) = 1000;
//  lqr_q(1,1) =  500;
//  lqr_q(2,2) =  300;
//  lqr_q(3,3) =  10;
//
//		    //// R matrix /////
//  lqr_r(0,0) =  200;
//  lqr_r(1,1) =  200;
//
//	      //// Calculate K matrix and update the P matrix /////
//  container_ = lqr_b.transpose() * lqr_p_i_1 * lqr_b + lqr_r;
//  K_ = - container_.inverse() * lqr_b.transpose() * lqr_p_i_1 * lqr_a;
//
//  lqr_p_i = lqr_a.transpose() * lqr_p_i_1 * lqr_a + lqr_q - lqr_a.transpose() * lqr_p_i_1 * lqr_b * container_.inverse() * lqr_b.transpose() * lqr_p_i_1 * lqr_a;
//
//	      //// control output tau ////
//  state_(0,0) = jointPos.vector()(1) - 0.0;
//  state_(0,1) = jointPos.vector()(2) - 1.57;
//  state_(0,2) = jointVel.vector()(1);
//  state_(0,3) = jointVel.vector()(2);
//
//  two_dof_tau_command_ = K_ * state_ + two_dof_gg;
//
//  tau_command_[1] = two_dof_tau_command_(0,0);
//  tau_command_[2] = two_dof_tau_command_(1,0);
//
//  ROS_INFO_STREAM("torque command_1: " << two_dof_mass(0,0));
//
//
//	      /// items for the next loop ///
//
//  lqr_p_i_1 = lqr_p_i;
//  two_dof_mass_former = two_dof_mass;



  }

  try{
    setActuatorCommands(tau_command_);
  }catch(const std::exception& e){
    MELO_ERROR_STREAM("[ContactDetectionCtrl::controlLoop] caught exception in setActuatorCommands:\n"
      << e.what());
    return false;
  }

  //Publish stuff
  eeFwdKin(xee_in_B_);
  if(publishEEPose_) publishEEPose();
  if(publishEETargetPose_) publishEETargetPose();

  Eigen::Matrix<double,6,1> poseError; //direction of error in B system
  poseDiff(xee_in_B_, xdes_in_B_, poseError);
  if(publishEEPoseError_) publishEEPoseError(-poseError);

  if(publishDirectCalculation_) publishDirectCalculation();

  if(publishGenMomentum_) publishGenMomentum();

  if(publishFirstOrderResidual_) publishFirstOrderResidual();

  if(publishSecondOrderResidual_) publishSecondOrderResidual();

  if(publishRealExternalTorque_) publishRealExternalTorque();

  return true;
}

bool ContactDetectionCtrl::gotoTarget(){

  //Obtain endeffector commands from highlevel controller
  huskanypulator_msgs::EEstate EE_state_target; //getState().getDesiredEEState();
  try{
    getStateMutex().lock();
    EE_state_target = getState().getDesiredEEState();
    getStateMutex().unlock();
  }catch (const std::exception& ex) {
    MELO_WARN("[ContactDetectionCtrl::gotoTarget] Unable to get the desiredEEState. %s", ex.what());
    return false;
  }

  if(EE_state_target.header.stamp == ros::Time(0.0)){
    MELO_WARN_THROTTLE(1.0, "[ContactDetectionCtrl::gotoTarget] Desired EE state not yet set.");
    return false;
  }

  /*
   * Transform from command frame (=reference frame of command message) to base frame
   */
  tf::StampedTransform T_base_command; //transform from command frame to world frame
  T_base_command.setIdentity();
  if (EE_state_target.header.frame_id != robotFrameId_){
    try {
      tfListener_->lookupTransform( robotFrameId_, EE_state_target.header.frame_id, ros::Time(0), T_base_command);
    }catch (tf::TransformException& ex) {
      MELO_WARN("[ContactDetectionCtrl::gotoTarget] Unable to get the requested transform. %s", ex.what());
      return false;
    }
  }

  huskanypulator_model::RotationQuaternion q_base_command(T_base_command.getRotation().getW(),
                                                          T_base_command.getRotation().getX(),
                                                          T_base_command.getRotation().getY(),
                                                          T_base_command.getRotation().getZ());
  huskanypulator_model::Position r_base_command_in_base(T_base_command.getOrigin().getX(),
                                                        T_base_command.getOrigin().getY(),
                                                        T_base_command.getOrigin().getZ());

  huskanypulator_model::RotationQuaternion q_command_EE(EE_state_target.pose.orientation.w,
                                                        EE_state_target.pose.orientation.x,
                                                        EE_state_target.pose.orientation.y,
                                                        EE_state_target.pose.orientation.z);
  huskanypulator_model::Position r_command_EE_in_command(EE_state_target.pose.position.x,
                                                         EE_state_target.pose.position.y,
                                                         EE_state_target.pose.position.z);

  huskanypulator_model::RotationQuaternion EE_target_orientation; //R_base,EE
  huskanypulator_model::Position EE_target_position;//r_base,EE in base
  try{
    if (EE_state_target.use_pose) {
      EE_target_orientation = q_base_command * q_command_EE;
      EE_target_position = r_base_command_in_base
          + q_base_command.rotate(r_command_EE_in_command);
    } else if (EE_state_target.use_twist) {
      // Get last desired pose.
      EE_target_position = xdes_in_B_.getPosition();
      EE_target_orientation = xdes_in_B_.getRotation();
      // Increment desired pose with twist.
      if ((ros::Time::now() - EE_state_target.header.stamp).toSec() < twistTimeoutMax_) {
        EE_target_position.x() += EE_state_target.twist.linear.x * dt_;
        EE_target_position.y() += EE_state_target.twist.linear.y * dt_;
        EE_target_position.z() += EE_state_target.twist.linear.z * dt_;

        Eigen::Vector3d twistAngular(EE_state_target.twist.angular.x,
                                     EE_state_target.twist.angular.y,
                                     EE_state_target.twist.angular.z);

        if (twistAngular.norm() > 1.0e-6) {
          Eigen::Quaterniond q_old(EE_target_orientation.w(),
                                   EE_target_orientation.x(),
                                   EE_target_orientation.y(),
                                   EE_target_orientation.z());
          Eigen::Quaterniond q_new_old(
              Eigen::AngleAxisd(twistAngular.norm() * dt_,
                                twistAngular.normalized()));
          Eigen::Quaterniond q_new = q_new_old * q_old;
          EE_target_orientation.w() = q_new.w();
          EE_target_orientation.x() = q_new.x();
          EE_target_orientation.y() = q_new.y();
          EE_target_orientation.z() = q_new.z();
        }
      } else {
        MELO_DEBUG_THROTTLE_STREAM(1.0, "[ContactDetectionCtrl::gotoTarget] twist command too old, set twist to zero.\n");
      }
    } else {
      EE_target_orientation = xee_in_B_.getRotation();
      EE_target_position = xee_in_B_.getPosition();
    }
  } catch(const std::exception& e) {
    MELO_ERROR_STREAM("[ContactDetectionCtrl::gotoTarget] caught exception when assigning target:\n"
      << e.what());
    return false;
  }

  huskanypulator_model::Pose EE_target_pose(EE_target_position, EE_target_orientation);

  /*
   * Now calculate the current error and reduce it to a max value in order to
   * avoid too large control actions.
   */
  Eigen::Matrix<double,6,1> poseError; //direction of error in base system: r_target,actual_in_base and Rvec_actual,target_in_base
  try{
     poseDiff(xee_in_B_, EE_target_pose, poseError);
  }catch(const std::exception& e){
    MELO_ERROR_STREAM("[ContactDetectionCtrl::gotoTarget] caught exception in poseDiff:\n"
      << e.what());
    return false;
  }

  double errorAngle = poseError.head<3>().norm();
  double errorNormPos = poseError.tail<3>().norm();

  if(errorNormPos < maxErrorThreshold_ and errorAngle*angleErrorScale_ < maxErrorThreshold_){
    try{
      // close enough to control velocity if requested
      xdes_in_B_ = EE_target_pose;
      if(EE_state_target.use_twist){
        xdes_in_B_dot_.getTranslationalVelocity() = q_base_command.rotate(
            huskanypulator_model::LinearVelocity(EE_state_target.twist.linear.x,
                                                 EE_state_target.twist.linear.y,
                                                 EE_state_target.twist.linear.z));
        xdes_in_B_dot_.getRotationalVelocity() = q_base_command.rotate(
            huskanypulator_model::LocalAngularVelocity(EE_state_target.twist.angular.x,
                                                       EE_state_target.twist.angular.y,
                                                       EE_state_target.twist.angular.z));
      }else{
        xdes_in_B_dot_.setZero();
      }
    }catch(const std::exception& e){
      MELO_ERROR_STREAM("[ContactDetectionCtrl::gotoTarget] caught exception in if:\n"
        << e.what());
      return false;
    }
  }else{
    try{
      //far away from goal pose -> reduce error terms to avoid large control actions
      //note: this only happens when pose control is requested
      xdes_in_B_dot_.setZero();
      if(errorAngle*angleErrorScale_ > maxErrorThreshold_){
        //reduce angle of error rotation and concatenate rotation des <- EE <- base)
        huskanypulator_model::RotationVector errorRotVecInv(-poseError.head<3>() * ((maxErrorThreshold_/angleErrorScale_) / errorAngle) ); //Rvec_actual,target~_in_base
        EE_target_orientation = huskanypulator_model::RotationQuaternion(
            xee_in_B_.getRotation() * xee_in_B_.getRotation().inverseRotate(errorRotVecInv)); //R_base,target~
      }
      if(errorNormPos > maxErrorThreshold_){
        //r_base,actual_in_base - r_target~,actual_in_base
        EE_target_position = huskanypulator_model::Position(xee_in_B_.getPosition().vector() - poseError.tail<3>()*(maxErrorThreshold_/errorNormPos));
      }
      xdes_in_B_ = huskanypulator_model::Pose(EE_target_position, EE_target_orientation);
    }catch(const std::exception& e){
      MELO_ERROR_STREAM("[ContactDetectionCtrl::gotoTarget] caught exception in else:\n"
        << e.what());
      return false;
    }
  }

  if (EE_state_target.use_accel){
    MELO_WARN_THROTTLE(2.0,"[HuskanypulatorController::gotoTarget] Setting acceleration has currently no effect");
  }
  return true;
}

void ContactDetectionCtrl::publishEEPose() {

  boost::shared_ptr<geometry_msgs::PoseStamped> poseMsg =
      boost::make_shared<geometry_msgs::PoseStamped>();

  poseMsg->pose.position.x    = xee_in_B_.getPosition().x();
  poseMsg->pose.position.y    = xee_in_B_.getPosition().y();
  poseMsg->pose.position.z    = xee_in_B_.getPosition().z();
  poseMsg->pose.orientation.w = xee_in_B_.getRotation().w();
  poseMsg->pose.orientation.x = xee_in_B_.getRotation().x();
  poseMsg->pose.orientation.y = xee_in_B_.getRotation().y();
  poseMsg->pose.orientation.z = xee_in_B_.getRotation().z();
  poseMsg->header.frame_id = "/base_link";
  poseMsg->header.stamp = ros::Time::now();
  eePosePublisher_.publish(poseMsg);
}

void ContactDetectionCtrl::publishEETargetPose() {

  geometry_msgs::PoseStampedPtr desiredPoseMsg =
      boost::make_shared<geometry_msgs::PoseStamped>();

  desiredPoseMsg->pose.position.x    = xdes_in_B_.getPosition().x();
  desiredPoseMsg->pose.position.y    = xdes_in_B_.getPosition().y();
  desiredPoseMsg->pose.position.z    = xdes_in_B_.getPosition().z();
  desiredPoseMsg->pose.orientation.w = xdes_in_B_.getRotation().w();
  desiredPoseMsg->pose.orientation.x = xdes_in_B_.getRotation().x();
  desiredPoseMsg->pose.orientation.y = xdes_in_B_.getRotation().y();
  desiredPoseMsg->pose.orientation.z = xdes_in_B_.getRotation().z();
  desiredPoseMsg->header.frame_id = "/base_link";
  desiredPoseMsg->header.stamp = ros::Time::now();
  eeTargetPosePublisher_.publish(desiredPoseMsg);
}

void ContactDetectionCtrl::publishEEPoseError(const Eigen::Matrix<double,6,1>& poseError) {

  kindr_msgs::VectorAtPositionPtr linearErrorMsg =
      boost::make_shared<kindr_msgs::VectorAtPosition>();
  kindr_msgs::VectorAtPositionPtr angularErrorMsg =
        boost::make_shared<kindr_msgs::VectorAtPosition>();

  linearErrorMsg->header.stamp = ros::Time::now();
  linearErrorMsg->header.frame_id = robotFrameId_;
  linearErrorMsg->type = kindr_msgs::VectorAtPosition::TYPE_VELOCITY;
  linearErrorMsg->name = "EEPoseErrorLinear";
  linearErrorMsg->vector.x = poseError(3);
  linearErrorMsg->vector.y = poseError(4);
  linearErrorMsg->vector.z = poseError(5);
  linearErrorMsg->position.x = xee_in_B_.getPosition().x();
  linearErrorMsg->position.y = xee_in_B_.getPosition().y();
  linearErrorMsg->position.z = xee_in_B_.getPosition().z();

  angularErrorMsg->header.stamp = linearErrorMsg->header.stamp;
  angularErrorMsg->header.frame_id = robotFrameId_;
  angularErrorMsg->type = kindr_msgs::VectorAtPosition::TYPE_ANGULAR_VELOCITY;
  angularErrorMsg->name = "EEPoseErrorAngular";
  angularErrorMsg->vector.x = poseError(0);
  angularErrorMsg->vector.y = poseError(1);
  angularErrorMsg->vector.z = poseError(2);
  angularErrorMsg->position.x = xee_in_B_.getPosition().x();
  angularErrorMsg->position.y = xee_in_B_.getPosition().y();
  angularErrorMsg->position.z = xee_in_B_.getPosition().z();

  eePoseErrorLinearPublisher_.publish(linearErrorMsg);
  eePoseErrorAngularPublisher_.publish(angularErrorMsg);
}


///////////////////////////////////////////// 2017.04.23 /////////////////////////////////////////////////////

void ContactDetectionCtrl::publishDirectCalculation() {

//////// Get joint position and joint velocity /////
//
  huskanypulator_model::JointPositions jointPos = getModel()->getState().getJointPositions();
  huskanypulator_model::JointVelocities jointVel = getModel()->getState().getJointVelocities();
  std::lock_guard<std::mutex> lock(desiredVelPosMutex_);
  huskanypulator_model::JointTorques jointTor = getModel()->getJointTorques();
//  getModel()->getMassInertiaMatrix(MassInertiaMatrix_);
//  MassInertiaMatrix_AJO_ = AJO_projection_.transpose() * MassInertiaMatrix_ * AJO_projection_;
//  getModel()->getNonlinearEffects(NonlinearEffects_);
//  NonlinearEffects_AJO_ = AJO_projection_.transpose() * NonlinearEffects_;
//
/////////////////////////////Direct Calculation method by using the model from MATLAB////////////////////////////////
//////// Define 2 dof mass matrix, c matrix and g matrix /////
////  Eigen::MatrixXd two_dof_mass(2,2);
////  Eigen::MatrixXd two_dof_cc(2,2);
////  Eigen::MatrixXd two_dof_gg(2,2);
////  two_dof_mass(0,0)=0.0095+1.384*0.0441+0.095+1.384*(0.16+0.0441+0.168* cos (jointPos.vector()(2)));
////  two_dof_mass(0,1)=0.0095+1.384*(0.0441+0.084* cos (jointPos.vector()(2)));
////  two_dof_mass(1,0)=0.0095+1.384*(0.0441+0.084* cos (jointPos.vector()(2)));
////  two_dof_mass(1,1)=0.0095+1.384*0.0441;
////  two_dof_cc(0,0)=-1.384*0.4*0.21* sin (jointPos.vector()(2)) * jointVel.vector()(2);
////  two_dof_cc(0,1)=-1.384*0.4*0.21* sin (jointPos.vector()(2)) * (jointVel.vector()(1)+jointVel.vector()(2));
////  two_dof_cc(1,0)=1.384*0.4*0.21* sin (jointPos.vector()(2)) * jointVel.vector()(1);
////  two_dof_cc(1,1)=0;
////  two_dof_gg(0,0)=getModel()->getGravityTerms().segment<6>(6)(1);
////  two_dof_gg(1,0)=getModel()->getGravityTerms().segment<6>(6)(2);
////////// Define generalized momentum ////
////  Eigen::MatrixXd two_dof_vel(2,1);
////  two_dof_vel(0,0)=jointVel.vector()(1);
////  two_dof_vel(1,0)=jointVel.vector()(2);
////  genMomentum_AJO_ = two_dof_mass * two_dof_vel;
////////// Direct calculation of external torque ////
////  external_torque_=-tau_readings_AJO_prev_ + (genMomentum_AJO_-genMomentum_AJO_prev_)/dt_ - two_dof_cc.transpose() * two_dof_vel + two_dof_gg;
//
//
///////////////////////////Direct Calculation method by using the model from rbdl/////////////////////////////////////////
//
//
//////Get the 2 dof mass matrix from the 6 dof model ////

  two_dof_mass(0,0) = MassInertiaMatrix_AJO_(1,1);
  two_dof_mass(0,1) = MassInertiaMatrix_AJO_(1,2);
  two_dof_mass(1,0) = MassInertiaMatrix_AJO_(2,1);
  two_dof_mass(1,1) = MassInertiaMatrix_AJO_(2,2);

////Calculate c matrix, christoffel symbols of the first type, from the mass matrix ////

  ///get properties from the previous mass matrix ////

  two_dof_mass_prev(0,0) = MassInertiaMatrix_AJO_prev_(1,1);
  two_dof_mass_prev(0,1) = MassInertiaMatrix_AJO_prev_(1,2);
  two_dof_mass_prev(1,0) = MassInertiaMatrix_AJO_prev_(2,1);
  two_dof_mass_prev(1,1) = MassInertiaMatrix_AJO_prev_(2,2);

  ////Calculate the generalized momentum ////

   two_dof_vel(0,0)=jointVel.vector()(1);
   two_dof_vel(1,0)=jointVel.vector()(2);
   genMomentum_AJO_ = two_dof_mass * two_dof_vel;
  ///calculate the c matrix of christoffel symbols of the first type ////
  ///get the nonlinear dynamic terms ////

  if(jointPos.vector()(2) - jointPosition_prev_[2] < 0.001){
	  two_dof_cc(0,0) = 0;
	  two_dof_cc(0,1) = 0;
	  two_dof_cc(1,0) = 0;
	  two_dof_cc(1,1) = 0;
  }
  else{
	  two_dof_cc(0,0) = 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2)) * jointVel.vector()(2);
	  two_dof_cc(1,0) = (( two_dof_mass_prev(1,0) - two_dof_mass(1,0) )/(jointPosition_prev_[1] - jointPos.vector()(1)) - 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2))) * jointVel.vector()(1);
	  two_dof_cc(0,1) = 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2)) * jointVel.vector()(1) +  (( two_dof_mass_prev(0,1) - two_dof_mass(0,1) )/(jointPosition_prev_[2] - jointPos.vector()(2)) - 0.5 * ( two_dof_mass_prev(1,1) - two_dof_mass(1,1) )/( jointPosition_prev_[1] - jointPos.vector()(1))) * jointVel.vector()(2);
	  two_dof_cc(1,1) = 0;
  }

   non_linear_model(0) = NonlinearEffects_AJO_(1);
   non_linear_model(1) = NonlinearEffects_AJO_(2);

//   two_dof_vel_matrix = two_dof_vel * two_dof_vel.transpose();

//   non_linear_term = two_dof_vel_matrix.inverse() * two_dof_vel_matrix * non_linear_model;

//////Get the gravity term for the 2 dof model ////

   two_dof_gg(0,0)=getModel()->getGravityTerms().segment<6>(6)(1);
   two_dof_gg(1,0)=getModel()->getGravityTerms().segment<6>(6)(2);



/////Direct calculation of extrernal torque ////

   tau_readings_(0,0) = jointTor.vector()(1);
   tau_readings_(1,0) = jointTor.vector()(2);
//
////   ROS_INFO_STREAM("Joint torque_1: " << jointTor.vector()(1));
//
   external_torque_=-tau_readings_ + (genMomentum_AJO_-genMomentum_AJO_prev_)/dt_ - two_dof_cc.transpose() * two_dof_vel + two_dof_gg;
//   external_torque_=-tau_readings_ + (genMomentum_AJO_-genMomentum_AJO_prev_)/dt_ - non_linear_model + two_dof_gg;

//  ROS_INFO_STREAM("two_dof_mass(0,0): " << MassInertiaMatrix_AJO_(1,1));
//  ROS_INFO_STREAM("two_dof_mass(0,1): " << two_dof_mass(0,1));
//  ROS_INFO_STREAM("two_dof_mass(1,0): " << two_dof_mass(1,0));
//  ROS_INFO_STREAM("two_dof_mass(1,1): " << two_dof_mass(1,1));


//////// Direct Calculation method result publisher ////
//
  geometry_msgs::Vector3StampedPtr directCalculationMsg=
      boost::make_shared<geometry_msgs::Vector3Stamped>();

  directCalculationMsg->header.stamp    = ros::Time::now();
  directCalculationMsg->header.frame_id = robotFrameId_;
  directCalculationMsg->vector.x        = external_torque_(0,0);
  directCalculationMsg->vector.y        = external_torque_(1,0);
  directCalculationMsg->vector.z        = external_torque_(0,0) + external_torque_(1,0);

  directCalculationPublisher_.publish(directCalculationMsg);

  //prepare for next time step

  MassInertiaMatrix_AJO_prev_ = MassInertiaMatrix_AJO_;
  genMomentum_AJO_prev_ = genMomentum_AJO_;

  jointPosition_prev_(0,0) = jointPos.vector()(1);
  jointPosition_prev_(1,0) = jointPos.vector()(2);



///////////////////////////////////////// 6 dof implementation /////////////////////////////////////////////





}

//////////////////2017.4.27 Design the 1st-order residual calculator ////

void ContactDetectionCtrl::publishFirstOrderResidual(){


  huskanypulator_model::JointPositions jointPos = getModel()->getState().getJointPositions();
  huskanypulator_model::JointVelocities jointVel = getModel()->getState().getJointVelocities();
  std::lock_guard<std::mutex> lock(desiredVelPosMutex_);
  huskanypulator_model::JointTorques jointTor = getModel()->getJointTorques();
//  getModel()->getMassInertiaMatrix(MassInertiaMatrix_);
//  MassInertiaMatrix_AJO_ = AJO_projection_.transpose() * MassInertiaMatrix_ * AJO_projection_;
//  getModel()->getNonlinearEffects(NonlinearEffects_);
//  NonlinearEffects_AJO_ = AJO_projection_.transpose() * NonlinearEffects_;

  //////Get the 2 dof mass matrix from the 6 dof model ////

  two_dof_mass(0,0) = MassInertiaMatrix_AJO_(1,1);
  two_dof_mass(0,1) = MassInertiaMatrix_AJO_(1,2);
  two_dof_mass(1,0) = MassInertiaMatrix_AJO_(2,1);
  two_dof_mass(1,1) = MassInertiaMatrix_AJO_(2,2);

  //////Calculate c matrix, christoffel symbols of the first type, from the mass matrix ////

    ///get properties from the previous mass matrix ////

  two_dof_mass_prev(0,0) = MassInertiaMatrix_AJO_prev_(1,1);
  two_dof_mass_prev(0,1) = MassInertiaMatrix_AJO_prev_(1,2);
  two_dof_mass_prev(1,0) = MassInertiaMatrix_AJO_prev_(2,1);
  two_dof_mass_prev(1,1) = MassInertiaMatrix_AJO_prev_(2,2);
    ///calculate the c matrix of christoffel symbols of the first type ////

  if(jointPos.vector()(2) - jointPosition_prev_[2] < 0.001){
  	two_dof_cc(0,0) = 0;
  	two_dof_cc(0,1) = 0;
  	two_dof_cc(1,0) = 0;
  	two_dof_cc(1,1) = 0;
  }
  else{
  	two_dof_cc(0,0) = 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2)) * jointVel.vector()(2);
  	two_dof_cc(1,0) = (( two_dof_mass_prev(1,0) - two_dof_mass(1,0) )/(jointPosition_prev_[1] - jointPos.vector()(1)) - 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2))) * jointVel.vector()(1);
  	two_dof_cc(0,1) = 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2)) * jointVel.vector()(1) +  (( two_dof_mass_prev(0,1) - two_dof_mass(0,1) )/(jointPosition_prev_[2] - jointPos.vector()(2)) - 0.5 * ( two_dof_mass_prev(1,1) - two_dof_mass(1,1) )/( jointPosition_prev_[1] - jointPos.vector()(1))) * jointVel.vector()(2);
  	two_dof_cc(1,1) = 0;
  }
//  two_dof_cc(0,0) = NonlinearEffects_AJO_(1,0);
//  two_dof_cc(1,0) = NonlinearEffects_AJO_(2,0);
  //////Get the gravity term for the 2 dof model ////

  two_dof_gg(0,0)=getModel()->getGravityTerms().segment<6>(6)(1);
  two_dof_gg(1,0)=getModel()->getGravityTerms().segment<6>(6)(2);

  ////Calculate the generalized momentum ////

  two_dof_vel(0,0)=jointVel.vector()(1);
  two_dof_vel(1,0)=jointVel.vector()(2);
  genMomentum_AJO_ = two_dof_mass * two_dof_vel;

  /////Direct calculation of extrernal torque ////

  tau_readings_(0,0) = jointTor.vector()(1);
  tau_readings_(1,0) = jointTor.vector()(2);


	/////First order residual calculator ////

  intergral_term_ = intergral_term_prev_ + (-tau_readings_ - two_dof_cc.transpose() * two_dof_vel + two_dof_gg - first_order_residual_prev_);
//  intergral_term_ = intergral_term_prev_ + (-tau_readings_ - two_dof_cc + two_dof_gg - first_order_residual_prev_);

  first_order_residual_ = 20 * dt_ * intergral_term_ + 20 * genMomentum_AJO_;


//  ROS_INFO_STREAM("Joint torque_1: " << tau_readings_(0,0));
//  ROS_INFO_STREAM("momentum(1,1): " << genMomentum_AJO_(0,0));



	////// First Order Residual method result publisher ////


  geometry_msgs::Vector3StampedPtr firstOrderResidualMsg=
      boost::make_shared<geometry_msgs::Vector3Stamped>();
  firstOrderResidualMsg->header.stamp    = ros::Time::now();
  firstOrderResidualMsg->header.frame_id = robotFrameId_;
  firstOrderResidualMsg->vector.x        = first_order_residual_(0,0);
  firstOrderResidualMsg->vector.y        = first_order_residual_(1,0);
  firstOrderResidualMsg->vector.z        = first_order_residual_(0,0) + first_order_residual_(1,0);

  firstOrderResidualPublisher_.publish(firstOrderResidualMsg);

	  //prepare for next time step

  MassInertiaMatrix_AJO_prev_ = MassInertiaMatrix_AJO_;
  jointPosition_prev_(0,0) = jointPos.vector()(1);
  jointPosition_prev_(1,0) = jointPos.vector()(2);

  first_order_residual_prev_ = first_order_residual_;
  intergral_term_prev_ = intergral_term_;


///////////////////////////////////// 6 dof implemenattion /////////////////////////////////////////////////
}


void ContactDetectionCtrl::publishSecondOrderResidual(){

  huskanypulator_model::JointPositions jointPos = getModel()->getState().getJointPositions();
  huskanypulator_model::JointVelocities jointVel = getModel()->getState().getJointVelocities();
  std::lock_guard<std::mutex> lock(desiredVelPosMutex_);
  huskanypulator_model::JointTorques jointTor = getModel()->getJointTorques();
//  getModel()->getMassInertiaMatrix(MassInertiaMatrix_);
//  MassInertiaMatrix_AJO_ = AJO_projection_.transpose() * MassInertiaMatrix_ * AJO_projection_;
//  getModel()->getNonlinearEffects(NonlinearEffects_);
//  NonlinearEffects_AJO_ = AJO_projection_.transpose() * NonlinearEffects_;

	  //////Get the 2 dof mass matrix from the 6 dof model ////

  two_dof_mass(0,0) = MassInertiaMatrix_AJO_(1,1);
  two_dof_mass(0,1) = MassInertiaMatrix_AJO_(1,2);
  two_dof_mass(1,0) = MassInertiaMatrix_AJO_(2,1);
  two_dof_mass(1,1) = MassInertiaMatrix_AJO_(2,2);

	  //////Calculate c matrix, christoffel symbols of the first type, from the mass matrix ////

	    ///get properties from the previous mass matrix ////

  two_dof_mass_prev(0,0) = MassInertiaMatrix_AJO_prev_(1,1);
  two_dof_mass_prev(0,1) = MassInertiaMatrix_AJO_prev_(1,2);
  two_dof_mass_prev(1,0) = MassInertiaMatrix_AJO_prev_(2,1);
  two_dof_mass_prev(1,1) = MassInertiaMatrix_AJO_prev_(2,2);
	    ///calculate the c matrix of christoffel symbols of the first type ////
  if(jointPos.vector()(2) - jointPosition_prev_[2] < 0.001){
    two_dof_cc(0,0) = 0;
	two_dof_cc(0,1) = 0;
	two_dof_cc(1,0) = 0;
	two_dof_cc(1,1) = 0;
  }
  else{
	two_dof_cc(0,0) = 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2)) * jointVel.vector()(2);
	two_dof_cc(1,0) = (( two_dof_mass_prev(1,0) - two_dof_mass(1,0) )/(jointPosition_prev_[1] - jointPos.vector()(1)) - 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2))) * jointVel.vector()(1);
	two_dof_cc(0,1) = 0.5 * ( two_dof_mass_prev(0,0) - two_dof_mass(0,0) )/( jointPosition_prev_[2] - jointPos.vector()(2)) * jointVel.vector()(1) +  (( two_dof_mass_prev(0,1) - two_dof_mass(0,1) )/(jointPosition_prev_[2] - jointPos.vector()(2)) - 0.5 * ( two_dof_mass_prev(1,1) - two_dof_mass(1,1) )/( jointPosition_prev_[1] - jointPos.vector()(1))) * jointVel.vector()(2);
	two_dof_cc(1,1) = 0;
  }
//  two_dof_cc(0,0) = NonlinearEffects_AJO_(1,0);
//  two_dof_cc(1,0) = NonlinearEffects_AJO_(2,0);
	  //////Get the gravity term for the 2 dof model ////
  two_dof_gg(0,0)=getModel()->getGravityTerms().segment<6>(6)(1);
  two_dof_gg(1,0)=getModel()->getGravityTerms().segment<6>(6)(2);

	  ////Calculate the generalized momentum ////

  two_dof_vel(0,0)=jointVel.vector()(1);
  two_dof_vel(1,0)=jointVel.vector()(2);
  genMomentum_AJO_ = two_dof_mass * two_dof_vel;

	  /////Direct calculation of extrernal torque ////

  tau_readings_(0,0) = jointTor.vector()(1);
  tau_readings_(1,0) = jointTor.vector()(2);


		///Second order residual calculator ////

  intergral_term_ = intergral_term_prev_ + (-tau_readings_ - two_dof_cc.transpose() * two_dof_vel + two_dof_gg - first_order_residual_prev_);

//  intergral_term_ = intergral_term_prev_ + (-tau_readings_ - two_dof_cc + two_dof_gg - first_order_residual_prev_);

  first_order_residual_ = 20 * dt_ * intergral_term_ + 20 * genMomentum_AJO_;

  intergral_term_two_ = intergral_term_two_prev_ + first_order_residual_ - second_order_residual_prev_;

  second_order_residual_ = 80 * dt_ * intergral_term_two_;

		////// Second Order Residual method result publisher ////

  geometry_msgs::Vector3StampedPtr secondOrderResidualMsg=
		  boost::make_shared<geometry_msgs::Vector3Stamped>();
  secondOrderResidualMsg->header.stamp    = ros::Time::now();
  secondOrderResidualMsg->header.frame_id = robotFrameId_;
  secondOrderResidualMsg->vector.x        = second_order_residual_(0,0);
  secondOrderResidualMsg->vector.y        = second_order_residual_(1,0);
  secondOrderResidualMsg->vector.z        = second_order_residual_(0,0) + second_order_residual_(1,0);

  secondOrderResidualPublisher_.publish(secondOrderResidualMsg);

		  //prepare for next time step
  MassInertiaMatrix_AJO_prev_ = MassInertiaMatrix_AJO_;
  jointPosition_prev_(0,0) = jointPos.vector()(1);
  jointPosition_prev_(1,0) = jointPos.vector()(2);

  first_order_residual_prev_ = first_order_residual_;
  second_order_residual_prev_ = second_order_residual_;

  intergral_term_prev_ = intergral_term_;
  intergral_term_two_prev_ = intergral_term_two_;


/////////////////////////////////////// 6 dof implementation ////////////////////////////////////////////////

}




void ContactDetectionCtrl::publishRealExternalTorque(){

  huskanypulator_model::JointPositions jointPos = getModel()->getState().getJointPositions();
  huskanypulator_model::JointVelocities jointVel = getModel()->getState().getJointVelocities();
  std::lock_guard<std::mutex> lock(desiredVelPosMutex_);

  J_ee_in_B_.setZero();
  getModel()->getJacobianSpatialWorldToBody(
    J_ee_in_B_,
	huskanypulator_model::BranchEnum::ARM,
	huskanypulator_model::BodyNodeEnum::TOOL,
	huskanypulator_model::CoordinateFrame::WORLD);



  Eigen::Matrix3d rotationWorldToBase = getModel()->getOrientationWorldToBody(
	huskanypulator_model::BodyEnum::BASE);
  J_ee_in_B_.topRows(3) = rotationWorldToBase * J_ee_in_B_.topRows(3);
  J_ee_in_B_.bottomRows(3) = rotationWorldToBase * J_ee_in_B_.bottomRows(3);

  J_ee_in_B_AJO_ = J_ee_in_B_ * AJO_projection_;

/////////////////////Get the 2 dof Jacobian ///////////////////////////////////


//  ROS_INFO_STREAM("Jacobian_entry_(1,1): " << two_dof_jacobian(0,0));
  if( second_order_residual_(0,0) < -0.8 && jointPos.vector()(2) < 1.56){
	  external_force(0,0) = 0;
	  external_force(1,0) = 0;
	  external_force(2,0) = 0;
	  external_force(3,0) = 0;
	  external_force(4,0) = 0;
	  external_force(5,0) = 5.8;
  }
  else{
	  external_force(0,0) = 0;
	  external_force(1,0) = 0;
	  external_force(2,0) = 0;
	  external_force(3,0) = 0;
	  external_force(4,0) = 0;
	  external_force(5,0) = 0;
  }

  real_external_torque = J_ee_in_B_AJO_.transpose() * external_force;

 ////////////////// Publish the real external torque on joints /////////////////

  geometry_msgs::Vector3StampedPtr realExternalTorqueMsg=
		  boost::make_shared<geometry_msgs::Vector3Stamped>();
  realExternalTorqueMsg->header.stamp    = ros::Time::now();
  realExternalTorqueMsg->header.frame_id = robotFrameId_;
  realExternalTorqueMsg->vector.x        = real_external_torque(1,0);
  realExternalTorqueMsg->vector.y        = real_external_torque(2,0);
  realExternalTorqueMsg->vector.z        = real_external_torque(1,0) + real_external_torque(2,0);

  realExternalTorquePublisher_.publish(realExternalTorqueMsg);

//  joint_2_posi_prev = jointPos.vector()(2);

}





void ContactDetectionCtrl::publishGenMomentum(){

  huskanypulator_model::JointPositions jointPos = getModel()->getState().getJointPositions();
  huskanypulator_model::JointVelocities jointVel = getModel()->getState().getJointVelocities();
  std::lock_guard<std::mutex> lock(desiredVelPosMutex_);
//  getModel()->getMassInertiaMatrix(MassInertiaMatrix_);
//  MassInertiaMatrix_AJO_ = AJO_projection_.transpose() * MassInertiaMatrix_ * AJO_projection_;


	  //////Get the 2 dof mass matrix from the 6 dof model ////

  two_dof_mass(0,0) = MassInertiaMatrix_AJO_(1,1);
  two_dof_mass(0,1) = MassInertiaMatrix_AJO_(1,2);
  two_dof_mass(1,0) = MassInertiaMatrix_AJO_(2,1);
  two_dof_mass(1,1) = MassInertiaMatrix_AJO_(2,2);

	  ////Calculate the generalized momentum ////
  two_dof_vel(0,0)=jointVel.vector()(1);
  two_dof_vel(1,0)=jointVel.vector()(2);
  genMomentum_AJO_ = two_dof_mass * two_dof_vel;

	   ////publish the generalized momentum ////
  geometry_msgs::Vector3StampedPtr genMomentumMsg=
      boost::make_shared<geometry_msgs::Vector3Stamped>();
  genMomentumMsg->header.stamp    = ros::Time::now();
  genMomentumMsg->header.frame_id = robotFrameId_;
  genMomentumMsg->vector.x        = genMomentum_AJO_(0,0);
  genMomentumMsg->vector.y        = genMomentum_AJO_(1,0);
  genMomentumMsg->vector.z        = genMomentum_AJO_(0,0) + genMomentum_AJO_(1,0);

  genMomentumPublisher_.publish(genMomentumMsg);


}

/////////////////////////////////////////////// 2017.04.23 /////////////////////////////////////////////////////




void ContactDetectionCtrl::getRotmatFromVector(const Eigen::Vector3d& vec, Eigen::Matrix3d& rotMat) const {
  if (vec.norm() < 1.0e-6){
    MELO_WARN("[ContactDetectionCtrl::getRotmatFromVector] Vector almost zero.");
  }

  const double threshold = 0.8;
  Eigen::Vector3d e_x_in_A, e_y_in_A, e_z_in_A; //unit vectors of frame B expressed in A

  e_x_in_A = vec.normalized();

  if((e_x_in_A.transpose()*Eigen::Vector3d::UnitZ()) < threshold){
    //vec sufficiently different from UnitZ
    e_y_in_A = vec.cross(Eigen::Vector3d::UnitZ());
  }else{
    //vec is close to UnitZ. choose UnitX instead
    e_y_in_A = vec.cross(Eigen::Vector3d::UnitX());
  }
  e_y_in_A.normalize();

  e_z_in_A = e_x_in_A.cross(e_y_in_A);
  e_z_in_A.normalize();//this should not really be necessary

  rotMat.col(0) = e_x_in_A;
  rotMat.col(1) = e_y_in_A;
  rotMat.col(2) = e_z_in_A;
}

void ContactDetectionCtrl::invKin() {
  MELO_INFO("invKin thread started");

  ros::WallRate rate(1000);

  Eigen::Matrix<double,6,1> e; //pose error in B system: actual - desired
  //initialize actuator positions
  huskanypulator_model::JointPositions desiredJointPos = getModel()->getState().getJointPositions();
  huskanypulator_model::JointVelocities desiredJointVel = getModel()->getState().getJointVelocities();
  desiredJointVelFiltered_.setZero();

  huskanypulator_model::Position r_IB_in_I(0.0, 0.0, 0.0);
  huskanypulator_model::RotationQuaternion orientationBaseToWorld;
  orientationBaseToWorld.setIdentity();
  huskanypulator_model::LinearVelocity linearVelocityBaseInWorldFrame(0.0, 0.0, 0.0);
  huskanypulator_model::LocalAngularVelocity angularVelocityBaseInBaseFrame(0.0, 0.0, 0.0);
  Eigen::Matrix<double,6,1> desiredJointSpeed;
  Eigen::Matrix<double,6,1> eePose;
  huskanypulator_model::HuskanypulatorState huskanypulatorState;

  while(runInvKin_){
    // set model state
    huskanypulatorState.setJointPositions(desiredJointPos);
    huskanypulatorState.setJointVelocities(desiredJointVel);
    huskanypulatorState.setPositionWorldToBaseInWorldFrame(r_IB_in_I);
    huskanypulatorState.setOrientationBaseToWorld(orientationBaseToWorld);
    huskanypulatorState.setLinearVelocityBaseInWorldFrame(linearVelocityBaseInWorldFrame);
    huskanypulatorState.setAngularVelocityBaseInBaseFrame(angularVelocityBaseInBaseFrame);
    invKinModel_.setState(huskanypulatorState, true, true, false);

    // forward kinematics
    huskanypulator_model::Position r_base_ee_in_base(
        invKinModel_.getPositionBodyToBody(huskanypulator_model::BodyEnum::BASE,
                                          huskanypulator_model::BodyEnum::TOOL,
                                          huskanypulator_model::CoordinateFrame::BASE));

    huskanypulator_model::RotationMatrix R_Base_World(
        invKinModel_.getOrientationWorldToBody(huskanypulator_model::BodyEnum::BASE));
    huskanypulator_model::RotationMatrix R_EE_World(
        invKinModel_.getOrientationWorldToBody(huskanypulator_model::BodyEnum::TOOL));


    huskanypulator_model::Pose fwdKinPose;
    fwdKinPose.getPosition() = r_base_ee_in_base;
    fwdKinPose.getRotation() = huskanypulator_model::RotationQuaternion(R_Base_World*(R_EE_World.transposed()));


    //compute pose error
    poseDiff(fwdKinPose, xdes_in_B_, e);
    if(e.norm() > 0.25){
      MELO_INFO_THROTTLE_STREAM(0.5, "e:\n" << e);
      MELO_INFO_THROTTLE_STREAM(0.5, "e.norm():\n" << e.norm());
      MELO_INFO_THROTTLE_STREAM(0.5, "fwdKinPose.getRotation():\n" << fwdKinPose.getRotation());
      MELO_INFO_THROTTLE_STREAM(0.5, "xdes_in_B_.getRotation():\n" << xdes_in_B_.getRotation());
      ROS_WARN_THROTTLE(0.5, "ContactDetectionCtrl::invKin() not converged. Estop");
      invKinConverged_ = false;
    }else{
      invKinConverged_ = true;
    }

    MELO_DEBUG_THROTTLE_STREAM(1.0, "e:\n" << e);

    //get jacobian
    Eigen::MatrixXd Jac_spatial;
    Jac_spatial.setZero(6,14);
    invKinModel_.getJacobianSpatialWorldToBody(
          Jac_spatial,
          huskanypulator_model::BranchEnum::ARM,
          huskanypulator_model::BodyNodeEnum::TOOL,
          huskanypulator_model::CoordinateFrame::BASE);


    Eigen::JacobiSVD<Eigen::MatrixXd::PlainObject> svd;
    svd.compute(Jac_spatial.block<6,6>(0,6), Eigen::ComputeThinU|Eigen::ComputeThinV);
    auto singularValues = svd.singularValues();
    Eigen::Matrix<double,6,1> Sigma = Eigen::Matrix<double,6,1>::Zero();

    assert(singularValues.size() == 6);
    for(int i=0; i<6; i++){
        const double threshold = 0.03;
        if(singularValues(i) < threshold){
          singularValues(i) = 0.0;
          Sigma(i) = 1.0;
        }
        else{
          singularValues(i) = 1.0/singularValues(i); //INVERSE
        }
    }

    //rebuild jacobian with small singular values set to zero
    Eigen::MatrixXd Jac_spatial_inv = svd.matrixV() * singularValues.asDiagonal() * svd.matrixU().transpose();
    Eigen::Matrix<double,6,6> P = svd.matrixV() * Sigma.asDiagonal() * svd.matrixU().transpose();

    //desired configuration
    Eigen::Matrix<double,6,1> q_des;
    q_des << -4.0*M_PI_4, 0.0, -M_PI_2, 3*M_PI_4, 0.0, 0.0;

    //Gains and jacobian inverse/transpose
    Eigen::Matrix<double,6,1> q_null;
    q_null.setZero();
    q_null(0) = -0.005 * (desiredJointPos.vector()(0) - q_des(0));
    q_null(1) = -0.005 * (desiredJointPos.vector()(1) - q_des(1));
    q_null(2) = q_null(3) = -0.005 * (desiredJointPos.vector()(1) + desiredJointPos.vector()(2) +desiredJointPos.vector()(3));
    Eigen::Matrix<double,6,1> K = Eigen::Matrix<double,6,1>::Ones()*0.005;
    desiredJointVel.vector().head<6>() = Jac_spatial_inv * K.asDiagonal() * (-e) + (Eigen::Matrix<double,6,6>::Identity() -Jac_spatial_inv*Jac_spatial.block<6,6>(0,6)) *q_null;
    desiredJointPos.vector().head<6>() = desiredJointPos.vector().head<6>() + desiredJointVel.vector().head<6>();

    //assign values
    {
      std::lock_guard<std::mutex> lock(desiredVelPosMutex_);
      desiredJointVel_ = desiredJointVel.vector().head<6>() * 1000.0;
      desiredJointVelFiltered_ = desiredJointVelFiltered_ + 0.001 * (desiredJointVel_ - desiredJointVelFiltered_);
      desiredJointPos_ = desiredJointPos.vector().head<6>();
    }
    MELO_DEBUG_THROTTLE_STREAM(1.0, "desired pos\n" << desiredJointPos_
        <<"\nactual joint pos\n" << getModel()->getState().getJointPositions());
    //sleep rest of the cycle time
    rate.sleep();
  }
}

} /* namespace huskanypulator_arm_controller */
